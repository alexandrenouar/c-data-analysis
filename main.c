/* Alexandre Nouar @Efrei 2019 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

struct node {
   int data;
   int key;
   struct node *next;
};

struct node *head = NULL; //pointeur global qui va nous etre utile pour initier la tete de liste
struct node *current = NULL; //pointeur global qui permet de faire pointer vers le noeud actuel, permettant de maniper la data

//Affichage des elements de la liste
void printList() {
   struct node *ptr = head;
   printf("\n[ ");

   //incrémenter
   while(ptr != NULL) {
      printf("(%d,%d) ",ptr->key,ptr->data);
      ptr = ptr->next;
   }

   printf(" ]");
}

//Insérer lien au premier endroit
void insertFirst(int key, int data) {
   //Création du lien
   struct node *link = (struct node*) malloc(sizeof(struct node));

   link->key = key;
   link->data = data;

   //pointeur vers l'ancien noeud
   link->next = head;

   //pointe vers le nouveau noeud
   head = link;
}

//pop item
struct node* deleteFirst() {

   //sauvegarde la reference du premier lien
   struct node *tempLink = head;

   //marque next à head pour faire du lien le premier
   head = head->next;

   //retourner le lien supprimé
   return tempLink;
}

//booléen, si la liste est vide
bool isEmpty() {
   return head == NULL;
}

int length() {
   int length = 0;
   struct node *current;

   for(current = head; current != NULL; current = current->next) {
      length++;
   }

   return length;
}

//trouve un lien avec une clé donnée
struct node* find(int key) {

   //commence avec le premier lien
   struct node* current = head;

   //if liste vide
   if(head == NULL) {
      return NULL;
   }

   //navigation au travers de la liste
   while(current->key != key) {

      //if dernier noeud
      if(current->next == NULL) {
         return NULL;
      } else {
         //aller au lien suivant
         current = current->next;
      }
   }

   //si de la data a été trouvée, on la retourne
   return current;
}

//suppression avec une clée donnée
struct node* delete(int key) {

   //on commence avec le premier lien
   struct node* current = head;
   struct node* previous = NULL;

   //si lien vide
   if(head == NULL) {
      return NULL;
   }

   //navigation au travers de la liste
   while(current->key != key) {

      //if dernier noeud
      if(current->next == NULL) {
         return NULL;
      } else {
         //stocker la reference au lien actuel
         previous = current;
         //et on bouge au lien suivant
         current = current->next;
      }
   }

   //key trouvée, on update le lien
   if(current == head) {
      //changer le premier pour le faire pointer sur next
      head = head->next;
   } else {
      //bypass le lien actuel
      previous->next = current->next;
   }

   return current;
}

void sort() {

   int i, j, k, tempKey, tempData;
   struct node *current;
   struct node *next;

   int size = length();
   k = size ;

   for ( i = 0 ; i < size - 1 ; i++, k-- ) {
      current = head;
      next = head->next;

      for ( j = 1 ; j < k ; j++ ) {

         if ( current->data > next->data ) {
            tempData = current->data;
            current->data = next->data;
            next->data = tempData;

            tempKey = current->key;
            current->key = next->key;
            next->key = tempKey;
         }

         current = current->next;
         next = next->next;
      }
   }
}

void reverse(struct node** head_ref) {
   struct node* prev   = NULL;
   struct node* current = *head_ref;
   struct node* next;

   while (current != NULL) {
      next  = current->next;
      current->next = prev;
      prev = current;
      current = next;
   }

   *head_ref = prev;
}

void main() {
    printf("%d",length);
   insertFirst(1,10);
   insertFirst(2,20);
   insertFirst(3,30);
   insertFirst(4,1);
   insertFirst(5,40);
   insertFirst(6,56);

   printf("La liste chainee contient les elements suivants ");

   printList();

   while(!isEmpty()) {
      struct node *temp = deleteFirst();
      printf("\n Valeur supprimee:");
      printf("(%d,%d) ",temp->key,temp->data);
   }

   printf("\n Liste apres suppression des elements ");
   printList();
   
   insertFirst(1,10);
   insertFirst(2,20);
   insertFirst(3,30);
   insertFirst(4,1);
   insertFirst(5,40);
   insertFirst(6,56);

   printf("\n Liste restauree : ");
   printList();
   printf("\n");

   struct node *foundLink = find(4);

   if(foundLink != NULL) {
      printf("Element trouve: ");
      printf("(%d,%d) ",foundLink->key,foundLink->data);
      printf("\n");
   } else {
      printf("Element non trouve.");
   }

   delete(4);
   printf("liste apres suppression de l'objet ");
   printList();
   printf("\n");
   foundLink = find(4);

   if(foundLink != NULL) {
      printf("Element trouve: ");
      printf("(%d,%d) ",foundLink->key,foundLink->data);
      printf("\n");
   } else {
      printf("Element non trouve");
   }

   printf("\n");
   sort();

   printf("Liste apres selection de la data ");
   printList();

   reverse(&head);
   printf("\ndonnee inversee ");
   printList();
}
